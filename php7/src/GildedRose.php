<?php

namespace App;

final class GildedRose
{

    private $items = [];

    const HIGHEST_QUALITY = 50;
    const LOWEST_QUALITY = 0;
    const LEGENDARY_QUALITY = 80;
    const SELL_IN_LIMIT = 0;
    const INCREASE_QUALITY_BY_TWO_LIMIT = 11;
    const INCREASE_QUALITY_BY_THREE_LIMIT = 6;
    const ITEM_AGED_BRIE = 'Aged Brie';
    const ITEM_BACKSTAGE = 'Backstage passes to a TAFKAL80ETC concert';
    const ITEM_CONJURED = 'Conjured Mana Cake';
    const ITEM_SULFURAS = 'Sulfuras, Hand of Ragnaros';

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function updateQuality()
    {
        foreach ($this->items as $item) {
            if ($this->isGetOlderGetBetter($item)) {
                $this->increaseQuality($item);
            } else {
                $this->decreaseQuality($item);
            }

            $this->decreaseSellInDate($item);

            if ($this->hasReachedSellInDate($item)) {
                $this->adaptQualitySellInDatePassed($item);
            }
        }
    }

    function decreaseSellInDate($item)
    {
        if ($item->name != self::ITEM_SULFURAS) {
            $item->sell_in = $item->sell_in - 1;
        }
    }

    function hasReachedSellInDate($item)
    {
        return $item->sell_in < self::SELL_IN_LIMIT;
    }

    function isGetOlderGetBetter($item)
    {
        return ($item->name == self::ITEM_AGED_BRIE || $item->name == self::ITEM_BACKSTAGE);
    }

    function increaseQuality($item)
    {
        if ($item->quality < self::HIGHEST_QUALITY) {
            $item->quality = $item->quality + 1;
            if ($item->name == self::ITEM_BACKSTAGE) {
                if ($item->sell_in < self::INCREASE_QUALITY_BY_TWO_LIMIT) {
                    if ($item->quality < self::HIGHEST_QUALITY) {
                        $item->quality = $item->quality + 1;
                    }
                }
                if ($item->sell_in < self::INCREASE_QUALITY_BY_THREE_LIMIT) {
                    if ($item->quality < self::HIGHEST_QUALITY) {
                        $item->quality = $item->quality + 1;
                    }
                }
            }
        }
    }

    function decreaseQuality($item)
    {
        if ($item->quality > self::LOWEST_QUALITY) {
            if ($item->name == self::ITEM_CONJURED) {
                $item->quality = $item->quality - 2;
            } else if ($item->name != self::ITEM_SULFURAS) {
                $item->quality = $item->quality - 1;
            } else {
                $item->quality = self::LEGENDARY_QUALITY;
            }
        }
    }

    function adaptQualitySellInDatePassed($item)
    {
        if ($item->name != self::ITEM_AGED_BRIE) {
            if ($item->name != self::ITEM_BACKSTAGE) {
                if ($item->quality > self::LOWEST_QUALITY) {
                    if ($item->name == self::ITEM_CONJURED) {
                        $item->quality = $item->quality - 2;
                    } else if ($item->name != self::ITEM_SULFURAS) {
                        $item->quality = $item->quality - 1;
                    }
                }
            } else {
                $item->quality = $item->quality - $item->quality;
            }
        } else {
            if ($item->quality < self::HIGHEST_QUALITY) {
                $item->quality = $item->quality + 1;
            }
        }
    }
}
