<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\GildedRose;
use App\Item;

echo "OMGHAI!\n";

$items = array(
    new Item('+5 Dexterity Vest', 10, 20),
    new Item(GildedRose::ITEM_AGED_BRIE, 2, 0),
    new Item('Elixir of the Mongoose', 5, 7),
    new Item(GildedRose::ITEM_SULFURAS, 0, 80),
    new Item(GildedRose::ITEM_SULFURAS, -1, 80),
    new Item(GildedRose::ITEM_BACKSTAGE, 15, 20),
    new Item(GildedRose::ITEM_BACKSTAGE, 10, 49),
    new Item(GildedRose::ITEM_BACKSTAGE, 5, 49),
    // this conjured item does not work properly yet
    new Item(GildedRose::ITEM_CONJURED, 2, 20)
);

$app = new GildedRose($items);

$days = 2;
if (count($argv) > 1) {
    $days = (int) $argv[1];
}

for ($i = 0; $i < $days; $i++) {
    echo ("-------- day $i --------\n");
    echo ("name, sellIn, quality\n");
    foreach ($items as $item) {
        echo $item . PHP_EOL;
    }
    echo PHP_EOL;
    $app->updateQuality();
}
